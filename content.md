# Docker - Lessons learned 

* Duration : 1 hour
* Demonstrate the lessons I learned from Docker and use 
  a small lab to demonstrate the key elements but also the
  important ones that can generate an extra cost if not well managed

## What is Docker ?

> Docker is an open platform for building, running and shipping distributed application.
- Docker uses LXC technology from the Linux world 
- A developper machine can mimic as close as possible the production environment
- The container image can be build by a continous devivery server

## Why everybody is excited about Docker ?

> Note : the next points came from Circleci.com 

* Dev-test-production equivalence
* Deploy any single application as a single binary artifact
* Efficient OS-level containerization

## What is a Docker Registry ?

* A central place to host your docker images
* Docker Hub is the offcial one, but anyone can create its own private registry
* Docker Hub handle [Repositories](http://docs.docker.com/docker-hub/userguide/repos.md) and [Automated Builds](http://docs.docker.com/docker-hub/userguide/builds.md)

## Docker - Networking

* Requires a learning curve
* Good to store complex configuration in predefined auto executable experiment like the
  whole [network topology](https://github.com/brandon-rhodes/fopnp/tree/m/playground) created by Brandon Rhodes 

## Docker Cache

* An important point to consider while choosing a Continous Integration Server
* Slow down the process of building the machine


## Docker - Boot2Docker

* `boot2docker init`s initialize the VirtualBox host machine for docker.
* > Changes outside of the /var/lib/docker and /var/lib/boot2docker 
  > directories will be lost after powering down or restarting 
  > the boot2docker VM.
  > (source)[https://github.com/boot2docker/boot2docker/blob/master/doc/FAQ.md#local-customisation-with-persistent-partition]

## Docker - Workflow

* Use boot2docker only for developpers
* Create or modify `/var/lib/boot2docker` - `bootsync.sh` and `bootlocal.sh` and to launch 
  respectively pre and post command. Do not forget to set the execute permissions on them.

## Docker - Local Registry

* Create a [Local Registry](https://docs.docker.com/articles/registry_mirror/)

## Tooling

* Despite the fact that docker-compose exists and helps a lot, it is still better
  to work with CLI and build/launch script for multi-container application.
* 

## Deployment

* Deploy a single binary image to dev / staging / production and improve the 
  way the system will behave in different environments

## Some security tips

* Never-ever store any sensitive data in (environment variables)[https://docs.docker.com/userguide/dockerlinks/#environment-variables] shared by all the containers 

## Ecosystem

* CircleCi is well integrated with Docker 
* Up to now CircleCi has ne real technical solution to take advantage of the Docker cache, 
  this slow down the process of the build

## CLI tips & Tricks

* `docker start`and `docker stop` does not need the full container id, just the first four letters